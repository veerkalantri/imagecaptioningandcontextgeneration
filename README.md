# Image Captioning And Context Generation

This is a project based on image captioning and context writing for a given input image. As there are many projects available for context writing but not a single project is available for image context generation i.e. the possible reason behind the image. We are using image discourse analysis and self-generated inference dataset for this project.

The dataset for training the model for caption generation is Flicker8k Dataset which contains around 8000 images and there is Flicker8kText dataset included along with that which is used for caption generation. This dataset contains 5 possible captions for each image in the dataset.

We have used 6000 images for training and 2000 for testing the model.

# Youtube: 
	https://www.youtube.com/watch?v=fBeQLLCM8Fg

# Project Mentor: 
	Mr. Animesh Tayal

# Project Members:  
	Mr. Anugrah Chimanekar (@anugrah-1)
	Mr. Veer Kalantri (@mads5)
	Mr. Mohit Adlakha (@mohitad21)
	Ms. Aditi Lokhande (@aditilokhande)
	Ms. Ayushi Mishra (@ayushi-m1)
